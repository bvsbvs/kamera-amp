<div class="productCont">
  <div>
      <amp-carousel lightbox width="400" height="300" layout="responsive" type="slides">
        <amp-img src="/img/sel-100mm-f2.8-stf-g-master-31.png" width="400" height="300" layout="responsive"></amp-img>
        <amp-img src="/img/sel-100mm-f2.8-stf-g-master-32.png" width="400" height="300" layout="responsive"></amp-img>
        <amp-img src="/img/sel-100mm-f2.8-stf-g-master-33.png" width="400" height="300" layout="responsive"></amp-img>
      </amp-carousel>
  </div>
  <div class="socialBtns">
    <div class="product-name">
      <h1>SEL 100mm F2.8 STF G-Master</h1>
    </div>
    <amp-social-share class="rounded"
      type="facebook"
      data-param-app_id="254325784911610"
      width="36"
      height="36"></amp-social-share>
    <amp-social-share class="rounded"
      type="gplus"
      width="36"
      height="36"></amp-social-share>
    <amp-social-share class="rounded"
      type="twitter"
      width="36"
      height="36"></amp-social-share>
  </div>
  <div class="price">
    <h4><span>Preis neu</span> 1.500,00 €</h4>
  </div>
</div>