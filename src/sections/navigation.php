<!-- Main navigation + sidebar-->  
<div class="mainNavigation top">
  
  <!-- Need add page-top button -->
  <a href="#">
    <amp-img 
      class="logoWrap"
      src="img/logo.png"
      height="20"
      width="125">
    </amp-img>
  </a>
    
  <button on="tap:sidebar-left.toggle"
    class="ampstart-btn" id="mainNav">
    <i class="fa fa-bars"></i>
  </button>
  <!-- Search -->
  <!-- Need add action property here -->
  <form method="GET" action="#" target="_top">
    <input name="search" type="search" placeholder="Search">
    <input type="submit" value="">
  </form>
</div>

<amp-sidebar id="sidebar-left"
  layout="nodisplay"
  side="left">
  <div role="button" aria-label="close sidebar" on="tap:sidebar-left.close" tabindex="0" class="ampstart-navbar-trigger items-start">✕</div>
  <nav toolbar="(min-width: 992px)"
    toolbar-target="target-element-left">
    <ul class="menuList">
      <li><a href="#">Mein Benutzerkonto</a></li>
      <li><a href="#">Mein Wunschzettel</a></li>
      <li><a href="#">Blog</a></li>
      <li><a href="#">Anmelden</a></li>
      <li><a href="#">Registrieren</a></li><hr>
      <li><a href="#">service@kamera.de</a></li>
    </ul>
  </nav>
</amp-sidebar>

<!-- Need for sidebar -->
<div id="target-element-left"></div>